//directory where the images are located
var imgDir = "/wp-content/plugins/tkt-leads/img/";


//Question 1 - Gender
var questionGender = "Ich bin...";

var answersGender = ["Weiblich", "Männlich"];


//Question 2 - Age
var questionAge = "Mein Alter in Jahren ist...";

var answersAge = ["Unter 30", "30 bis 49", "50 und mehr"];


//Question 3 - Goal
var questionGoal = "Ich interessiere mich am meisten für:";

var answersGoal = ["Figur und Körpergewicht", "Rücken und Gelenke kräftigen", "Ausdauer, Kraft und Beweglichkeit", "Fit, vital und gesund zu sein"];

var imagesGoal = ["mann-uebergewicht.jpg", "frau-rueckenschmerzen.jpg", "frau-yoga.jpg", "mann-frau-jogging.jpg"];


//Question 4 - Previous Experience wrt Goal
var questionsGoalExperience = ["Habe ich Erfahrungen mit Figur-Training und Diäten?", "Habe ich Erfahrungen mit Rücken- und Gelenke-Training?", "Habe ich Erfahrungen mit Ausdauer-, Kraft- und Beweglichkeits-Training?", "Habe ich Erfahrungen mit Fitness- und Gesundheits-Training?"];

var answersGoalExperience = ["Nein, keine", "Sehr wenige", "Ja, viele, ohne Erfolg"];

var imagesGoalExperience =["mann-achselzucken.jpg", "2-frauen-1-mann-workout.jpg", "mann-daumen-runter.jpg"];


//Question 5 - Goal Scope
var questionsGoalScope = ["Wieviel Gewicht und Körperfett möchte ich verlieren?", "Wieviel Entwicklungspotenzial sehe ich im Bereich Rücken und Gelenke?", "Wieviel Entwicklungspotenzial sehe ich im Bereich Ausdauer, Kraft und Beweglichkeit?", "Wieviel Entwicklungspotenzial sehe ich im Bereich fit, vital und gesund zu sein?"];

var answersGoalScope = ["Wenig", "Viel", "Sehr viel"];


//Question 6 - Weight Subgoal (only appears if first goal was chosen)
var questionSubgoal = "Dieser Bereich ist mir besonders wichtig:";

var answersSubgoal = ["Bauch", "Beine", "Po"];

var imagesSubgoal = ["frau-bauchumfang-messen.jpg", "frau-beine.jpg", "frau-po.jpg"];


//Question 7 - Goup or solitary
var questionGroup = "Am liebsten trainiere ich...";

var answersGroup = ["In Gemeinschaft.", "Lieber alleine."];

var imagesGroup = ["gruppe-junger-frauen-aerobic.jpg", "frau-yoga-sonnenuntergang.jpg"];


//Question 8 - Time
var questionTime = "Ich nehme mir dafür zukünftig in der Woche...";

var answersTime = ["1 bis 2 Stunden", "3 bis 4 Stunden", "mehr als 3 Stunden"];


//Question 9 - Budget
var questionBudget = "Mein finanzielles Wochen-Budget für meine Ziele liegt bei...";

var answersBudget = ["Weniger als 13€", "13€ bis 16€", "Mehr als 16€"];