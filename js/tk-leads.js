
var questionPath = []; //if this is empty, we are at the first question

var formal = false;

var chosenAnswerGender = "NOT_SET";
var chosenAnswerAge = "NOT_SET";
var chosenAnswerGoal = "NOT_SET";
var chosenAnswerGoalExperience = "NOT_SET";
var chosenAnswerGoalScope = "NOT_SET";
var chosenAnswerSubgoal = "NOT_SET";
var chosenAnswerGroup = "NOT_SET";
var chosenAnswerTime = "NOT_SET";
var chosenAnswerBudget = "NOT_SET";

var goal = "NOT_SET";

var goalText = "NOT_SET";
var goalExperienceText = "NOT_SET";
var goalBudgetText = "NOT_SET";
var goalTimeText = "NOT_SET";
var resultsGoalBasisPhrase = "NOT_SET";

function getNextQuestionID(currentID, nextID, goal) {
    switch (currentID) {
        case "tk-question-goal-scope":
            switch(goal){
                case "tk-goal-abnehmen":
                    return "tk-question-subgoal";
                default:
                    return nextID;
            }
        default:
            return nextID;
    }
}

function getHeadlineExperienceText(goal) {
    switch(goal) {
        case "tk-goal-abnehmen":
            return questionsGoalExperience[0]; //"Habe ich Erfahrungen mit Figur-Training und Diäten?";
        case "tk-goal-ruecken":
            return questionsGoalExperience[1]; //"Habe ich Erfahrungen mit Rücken- und Gelenke-Training?";
        case "tk-goal-ausdauer":
            return questionsGoalExperience[2]; //"Habe ich Erfahrungen mit Ausdauer-, Kraft- und Beweglichkeits-Training?";
        case "tk-goal-fitness":
            return questionsGoalExperience[3]; //"Habe ich Erfahrungen mit Fitness- und Gesundheits-Training?";
        default:
            return "ERROR: Goal not set correctly!";
    }
}

function getHeadlineScopeText(goal) {
    switch(goal) {
        case "tk-goal-abnehmen":
            return questionsGoalScope[0];//"Wieviel Gewicht und Körperfett möchte ich verlieren?";
        case "tk-goal-ruecken":
            return questionsGoalScope[1];//"Wieviel Entwicklungspotenzial sehe ich im Bereich Rücken und Gelenke?";
        case "tk-goal-ausdauer":
            return questionsGoalScope[2];//"Wieviel Entwicklungspotenzial sehe ich im Bereich Ausdauer, Kraft und Beweglichkeit?";
        case "tk-goal-fitness":
            return questionsGoalScope[3];//"Wieviel Entwicklungspotenzial sehe ich im Bereich Fit, vital und gesund zu sein?";
        default:
            return "ERROR: Goal not set correctly!";
    }
}

function getResultsGoalExperienceText(goalExperience){
    switch(goalExperience) {
        case "tk-answer-goal-experience-none":
            if(formal){
                return "Dass Sie Sich diesem Thema ganz neu widmen möchten, ist eine <b>große Chance</b> und die ersten <b>Erfolge werden nicht lange auf sich warten lassen.</b>";
            }else{
                return "Dass Du Dich diesem Thema ganz neu widmen möchtest, ist eine <b>große Chance</b> und die ersten <b>Erfolge werden nicht lange auf sich warten lassen.</b>";
            }
        case "tk-answer-goal-experience-low":
            if(formal){
                return "Schön, dass Sie schon <b>auf bewährtes Wissen aufbauen</b> können. So werden sich <b>sehr schnell tolle Ergebnisse</b> einstellen.";
            }else{
                return "Schön, dass Du schon <b>auf bewährtes Wissen aufbauen</b> kannst. So werden sich <b>sehr schnell tolle Ergebnisse</b> einstellen.";
            }
        case "tk-answer-goal-experience-high":
            if(formal){
                return "Sie haben schon vieles ausprobiert und die ein oder anderen <b>Erfahrungen gesammelt.</b> Nun endlich sollen Ihren Aktivitäten auch die <b>gewünschten Ergebnisse folgen</b>. Wir sorgen dafür, dass diese Erfolge auch <b>nachhaltig</b> sind.";
            }else{
                return "Du hast schon vieles ausprobiert und die ein oder anderen <b>Erfahrungen gesammelt.</b> Nun endlich sollen Deinen Aktivitäten auch die <b>gewünschten Ergebnisse folgen</b>. Wir sorgen dafür, dass diese Erfolge auch <b>nachhaltig</b> sind.";
            }
        default:
            return "ERROR: Goal experience not set correctly!";
    }
}

function getResultsGoalBasisPhrase(goalTime) {
    switch(goalTime) {
        case "tk-answer-time-low":
            if(formal){
                return "die <b>Grundlage</b> für Ihre <b>ersten Schritte zum Erfolg.</b>";
            }else{
                return "die <b>Grundlage</b> für Deine <b>ersten Schritte zum Erfolg.</b>";
            }
        case "tk-answer-time-medium":
            return "eine <b>sehr gute Grundlage</b> und führen somit zu <b>schnellen Erfolgen.</b>";
        case "tk-answer-time-high":
            return "eine <b>exzellente Grundlage</b> für <b>schnellen</b> und vor allem, <b>nachhaltigen Erfolg.</b>";
        default:
            return "ERROR: Time for goal not set correctly!";
    }
}

jQuery(document).ready(function ($) {
    $(document).one("ready", function () {

        formal = $(".tk-lead-formal").val();

        var answers;
        //INITIALIZE QUESTIONS/ANSWERS/IMAGES
        $(this).find("#tk-question-gender h2").text(questionGender);
        answers = $(this).find("#tk-question-gender .tk-answer");
        answers.each(function(index){
            $(this).text(answersGender[index]);
        });

        $(this).find("#tk-question-age h2").text(questionAge);
        answers = $(this).find("#tk-question-age .tk-answer");
        answers.each(function(index){
            $(this).text(answersAge[index]);
        });

        $(this).find("#tk-question-goal h2").text(questionGoal);
        $(this).find("#tk-question-goal .tk-image").attr("src", imgDir+imagesGoal[0]);
        answers = $(this).find("#tk-question-goal .tk-answer");
        answers.each(function(index){
            $(this).text(answersGoal[index]);
            $(this).data("img", imagesGoal[index]);
        });

        $(this).find("#tk-question-goal-experience .tk-image").attr("src", imgDir+imagesGoalExperience[0]);
        answers = $(this).find("#tk-question-goal-experience .tk-answer");
        answers.each(function(index){
            $(this).text(answersGoalExperience[index]);
            $(this).data("img", imagesGoalExperience[index]);
        });

        answers = $(this).find("#tk-question-goal-scope .tk-answer");
        answers.each(function(index){
            $(this).text(answersGoalScope[index]);
        });

        $(this).find("#tk-question-subgoal h2").text(questionSubgoal);
        $(this).find("#tk-question-subgoal .tk-image").attr("src", imgDir+imagesSubgoal[0]);
        answers = $(this).find("#tk-question-subgoal .tk-answer");
        answers.each(function(index){
            $(this).text(answersSubgoal[index]);
            $(this).data("img", imagesSubgoal[index]);
        });

        $(this).find("#tk-question-group h2").text(questionGroup);
        $(this).find("#tk-question-group .tk-image").attr("src", imgDir+imagesGroup[0]);
        answers = $(this).find("#tk-question-group .tk-answer");
        answers.each(function(index){
            $(this).text(answersGroup[index]);
            $(this).data("img", imagesGroup[index]);
        });

        $(this).find("#tk-question-time h2").text(questionTime);
        answers = $(this).find("#tk-question-time .tk-answer");
        answers.each(function(index){
            $(this).text(answersTime[index]);
        });

        $(this).find("#tk-question-budget h2").text(questionBudget);
        answers = $(this).find("#tk-question-budget .tk-answer");
        answers.each(function(index){
            $(this).text(answersBudget[index]);
        });
    });

    $(document).on('click', '.tk-answer', function () {

        var answers = $(this).parent();
        var answerSection = answers.parent();
        var question = answerSection.parent();

        $(this).addClass("tk-selected");

        //set image
        answerSection.find(".tk-image").attr("src", (imgDir+$(this).data("img")) );

        //record answers
        if(question.is("#tk-question-gender")){
            chosenAnswerGender = $(this).text();
        }
        if(question.is("#tk-question-age")){
            chosenAnswerAge = $(this).text();
        }
        if(question.is("#tk-question-goal")){
            chosenAnswerGoal = $(this).text();
            goal = $(this).data("answer");
            goalText = $(this).text();
        }
        if(question.is("#tk-question-goal-experience")){
            chosenAnswerGoalExperience = $(this).text();
            goalExperienceText = getResultsGoalExperienceText($(this).data("answer"));
        }
        if(question.is("#tk-question-goal-scope")){
            chosenAnswerGoalScope = $(this).text();
        }
        if(question.is("#tk-question-subgoal")){
            chosenAnswerSubgoal = $(this).text();
        }
        if(question.is("#tk-question-group")){
            chosenAnswerGroup = $(this).text();
        }
        if(question.is("#tk-question-time")){
            chosenAnswerTime = $(this).text();
            goalTimeText = $(this).text();
            resultsGoalBasisPhrase = getResultsGoalBasisPhrase($(this).data("answer"));
        }
        if(question.is("#tk-question-budget")){
            chosenAnswerBudget = $(this).text();
            goalBudgetText = $(this).text();
        }


         //move to next question
        questionPath.push(question);

        var nextQuestionID = $(this).data('next-question'); //default question path as defined by data attribute
        nextQuestionID = getNextQuestionID(question.attr("id"), nextQuestionID, goal); //modify question path according to goal
        var nextQuestion = $("#"+nextQuestionID);

        if(nextQuestionID === "tk-results-section"){
            var answersData =
                questionGender+" -> "+chosenAnswerGender+"\n\n"+
                questionAge+" -> "+chosenAnswerAge+"\n\n"+
                questionGoal+" -> "+chosenAnswerGoal+"\n\n"+
                getHeadlineExperienceText(goal)+" -> "+chosenAnswerGoalExperience+"\n\n"+
                getHeadlineScopeText(goal)+" -> "+chosenAnswerGoalScope+"\n\n";

            if (chosenAnswerSubgoal !== "NOT_SET")
                answersData += questionSubgoal+" -> "+chosenAnswerSubgoal+"\n\n";

            answersData +=
                questionGroup+" -> "+chosenAnswerGroup+"\n\n"+
                questionTime+" -> "+chosenAnswerTime+"\n\n"+
                questionBudget+" -> "+chosenAnswerBudget+"\n\n";
        }

        question.removeClass("tk-question-current");
        question.hide("slide", { direction: "left" }, 500, function () {

            var previousButton = $(".tk-previous-button");

            previousButton.prop("disabled", false);

            nextQuestion.find(".tk-question-headline-experience").text(getHeadlineExperienceText(goal));
            nextQuestion.find(".tk-question-headline-scope").text(getHeadlineScopeText(goal));

            if(nextQuestionID === "tk-results-section"){
                previousButton.hide();
                var results = $(".tk-results-hidden");
                results.find(".tk-results-goal").text(goalText);
                results.find(".tk-results-goal-experience").html(goalExperienceText);
                results.find(".tk-results-budget").text(goalBudgetText);
                results.find(".tk-results-time").text(goalTimeText);
                results.find(".tk-results-phrase").html(resultsGoalBasisPhrase);
                nextQuestion.find(".tk-form-answers").val(answersData);
                nextQuestion.find(".tk-form-results").val(results.html());
            }

            nextQuestion.show();
            nextQuestion.addClass("tk-question-current");
        });
    });

    $(document).on('click', '.tk-previous-button', function () {

        var question = $(".tk-question-current");

        var answer = question.find(".tk-answer.tk-selected");
        answer.removeClass("tk-selected");

        var lastQuestion = questionPath.pop();
        lastQuestion.find('.tk-selected').removeClass("tk-selected");

        question.removeClass("tk-question-current");
        question.hide("slide", { direction: "right" }, 500, function () {
            var previousButton = $(".tk-previous-button");
            if(lastQuestion.hasClass("tk-question-first")){
                previousButton.prop("disabled", true);
            }
            lastQuestion.show();
            lastQuestion.addClass("tk-question-current");
        });
    });

    $('.tk-answer').hoverIntent(
        function() {
            var answerSection = $(this).parent().parent();
            if (!answerSection.find(".tk-answer.tk-selected").size()) {
                answerSection.find(".tk-image").attr("src", (imgDir+$(this).data("img")) );
            }
        }
    );
});
