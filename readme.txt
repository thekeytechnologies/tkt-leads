Benutzung: Formular anlegen, neue Seite anlegen, Fragebogen-Shortcode mit Formular-Shortcode ins Content-Feld kopieren

[tk-lead-questionnaire studio_name="DAS BESTE STUDIO DER WELT" button_text_color_hover="white" button_background_color_hover="#C3311D" button_text_color_selected="white" button_background_color_selected="#C3311D" button_previous_text_color="black" button_previous_background_color="rgb(170, 170, 255)" formal="true"]HIER_KOMMT_FORMULAR_SHORTCODE_HIN[/tk-lead-questionnaire]


Beispielformular:

<label> Ihr Name (Pflichtfeld)
    [text* your-name] </label>

<label> Ihre E-Mail-Adresse (Pflichtfeld)
    [email* your-email] </label>

<label> Ihre Telefonnummer (Pflichtfeld)
    [tel* your-tel] </label>

[submit "Senden"]

[hidden tk-conversion-action "LeadgenFragebogen"]

[hidden answers class:tk-form-answers]

[hidden results class:tk-form-results]



--------------------------------------
Zugehörige E-Mail:

Von: [your-name] <[your-email]>
Betreff: [your-subject]

Name: [your-name]
E-Mail: [your-email]
Telefon: [your-tel]

Fragebogen-Antworten:


[answers]




Auto-Repsonse:
(HTML-Mail aktivieren)

[results]