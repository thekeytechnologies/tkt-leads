<?php
/*
Plugin Name: TKT Lead Tools
Plugin URI:  https://www.thekey.technology
Version:     1
Author:      the key technology
Author URI:  https://www.thekey.technology
License:     proprietary
Text Domain: tkt-leads
*/

function tkLeadsAddFiles()
{
    wp_enqueue_style('tkt-leads-style', '/wp-content/plugins/tkt-leads/css/tk-leads.css');
    wp_enqueue_script('hoverintent', '/wp-content/plugins/tkt-leads/js/hoverIntent.min.js');
    wp_enqueue_script('jquery-ui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js', array("jquery"));
    wp_enqueue_script('tkt-leads-config', '/wp-content/plugins/tkt-leads/js/tk-leads-config.js', array("hoverintent", "jquery-ui"));
    wp_enqueue_script('tkt-leads-script', '/wp-content/plugins/tkt-leads/js/tk-leads.js', array("tkt-leads-config", "hoverintent", "jquery-ui"));
}

add_action('wp_enqueue_scripts', 'tkLeadsAddFiles', 11);

add_shortcode("tk-lead-questionnaire", function ( $questionnaireAttributes , $formshortcode = '') {

    $questionnaireAttributes = shortcode_atts(
        array(
            'studio_name' => 'STUDIO',
            'button_text_color_hover' => 'white',
            'button_background_color_hover' => '#C3311D',
            'button_text_color_selected' => 'white',
            'button_background_color_selected' => '#C3311D',
            'button_previous_text_color' => '#888888',
            'button_previous_background_color' => '#22ff22',
            'formal' => false
        ),
            $questionnaireAttributes, 'tk-lead-questionnaire');
    $questionnaireAttributes['form_shortcode'] = $formshortcode;

    $templateEngine = new TkTemplate(__DIR__ . "/twig");
    tkAddWpDisplayFilter($templateEngine);
    return $templateEngine->renderTemplate("lead-questionnaire.twig", $questionnaireAttributes );
});

add_filter("wpcf7_mail_tag_replaced", function($replaced, $submitted, $html){
    if($html && startsWith($replaced, "___tk-rawhtml")){
        return wp_specialchars_decode(str_replace("___tk-rawhtml", "", $replaced));
    }else{
        return $replaced;
    }
}, 11, 3);